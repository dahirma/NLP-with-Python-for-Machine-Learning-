# NLP-with-Python-for-Machine-Learning

Instructor: Derek Jedamski

●	Machine Learning pipeline, 
●	tokenization, 
●	punctuation removal, 
●	stop word removal, 
●	stemming, 
●	lemmatizing, 
●	vectorizing, count vectorization, 
●	N-gram vectorization, 
●	Inverse document frequency weighting, 
●	feature engineering, feature transformation, 
●	Box-Cox power transformation, 
●	cross-validation and evaluation metrics, 
●	random forest, random forest model with grid search, 
●	gradient boosting, Gradient-boosting grid search, 
●	evaluate gradient-boosting model performance 
